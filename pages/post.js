import Layout from "../components/Layout";
import {withRouter} from 'next/router';


const Post = withRouter((props) => {
    return(

    <Layout>
        <h1>{props.router.query.title}</h1>
        <p style={{width: "80vw"}}>Donec scelerisque gravida elit vel blandit. Aenean libero erat, 
            laoreet egestas odio vitae, maximus euismod velit. Pellentesque 
            habitant morbi tristique senectus et netus et malesuada fames ac 
            turpis egestas. Quisque vel felis vitae velit pulvinar suscipit 
            a ut risus. Fusce eget tristique urna. Morbi blandit diam ut nisi 
            ultrices iaculis. Donec non magna non nulla ullamcorper dapibus. 
            Fusce condimentum scelerisque lacus, et pharetra elit condimentum et. 
            Proin faucibus lorem in orci pulvinar aliquet. Suspendisse vestibulum 
            lacinia velit, vehicula scelerisque ipsum faucibus a. Vestibulum 
            vitae quam nec nibh semper convallis in iaculis nibh.</p>
    </Layout>
    )
});

export default Post;
