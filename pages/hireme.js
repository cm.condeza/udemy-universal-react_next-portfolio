import Layout from "../components/Layout";


const Hireme = () => (

    <Layout title="Hire Me">
        <p>You can hire me at <a href="mailto:cm.condeza@gmail.com">
            cm.condeza@gmail.com</a> </p>
    </Layout>
);

export default Hireme;